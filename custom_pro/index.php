<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="keywords" content="Rubber, rubber products, rubber bands, pallet & mover bands, Straps,  sri lankan rubber manufacturer "/>
<meta name="description" content="JB Rubber Products was incorporated in the early 80's to manufacture extruded rubber products such as rubber bands and has since evolved into a dynamic manufacturer of both extruded and moulded products.">
<meta name="author" content="">
<link rel="icon" href="../resources/images/favicon.ico">

<title>JAF RUBBER | Rubber Products Manufacturer - Sri Lanka | CUSTOM PRODUCT DEVELOPMENT | CUSTOM RUBBER FORMULATION</title>

<link href="../resources/styles/main.css" type="text/css" rel="stylesheet" media="screen" />

<script type="text/javascript" src="../resources/scripts/jquery-1.7.1.min.js"></script>
<link rel="stylesheet" type="text/css" href="../resources/styles/ddsmoothmenu.css" />

<script type="text/javascript" src="../resources/scripts/ddsmoothmenu.js">
/***********************************************
* Smooth Navigational Menu- (c) Dynamic Drive DHTML code library (www.dynamicdrive.com)
* This notice MUST stay intact for legal use
* Visit Dynamic Drive at http://www.dynamicdrive.com/ for full source code
***********************************************/
</script>
<script type="text/javascript">
ddsmoothmenu.init({
	mainmenuid: "smoothmenu1", //menu DIV id
	orientation: 'h', //Horizontal or vertical menu: Set to "h" or "v"
	classname: 'ddsmoothmenu', //class added to menu's outer DIV
	//customtheme: ["#1c5a80", "#18374a"],
	contentsource: "markup" //"markup" or ["container_id", "path_to_menu_file"]
})
</script>

<link rel="stylesheet" type="text/css" href="../resources/styles/products_rightmenu.css" />
<script type="text/javascript" src="../resources/scripts/ddaccordion.js">
/***********************************************
* Accordion Content script- (c) Dynamic Drive DHTML code library (www.dynamicdrive.com)
* Visit http://www.dynamicDrive.com for hundreds of DHTML scripts
* This notice must stay intact for legal use
***********************************************/
</script>
<script type="text/javascript">
ddaccordion.init({
	headerclass: "submenuheader", //Shared CSS class name of headers group
	contentclass: "submenu", //Shared CSS class name of contents group
	revealtype: "click", //Reveal content when user clicks or onmouseover the header? Valid value: "click", "clickgo", or "mouseover"
	mouseoverdelay: 200, //if revealtype="mouseover", set delay in milliseconds before header expands onMouseover
	collapseprev: true, //Collapse previous content (so only one open at any time)? true/false 
	defaultexpanded: [], //index of content(s) open by default [index1, index2, etc] [] denotes no content
	onemustopen: false, //Specify whether at least one header should be open always (so never all headers closed)
	animatedefault: false, //Should contents open by default be animated into view?
	persiststate: false, //persist state of opened contents within browser session?
	toggleclass: ["", ""], //Two CSS classes to be applied to the header when it's collapsed and expanded, respectively ["class1", "class2"]
	togglehtml: ["suffix", "<img src='../resources/images/plus.gif' class='statusicon' />", "<img src='../resources/images/minus.gif' class='statusicon' />"], //Additional HTML added to the header when it's collapsed and expanded, respectively  ["position", "html1", "html2"] (see docs)
	animatespeed: "fast", //speed of animation: integer in milliseconds (ie: 200), or keywords "fast", "normal", or "slow"
	oninit:function(headers, expandedindices){ //custom code to run when headers have initalized
		//do nothing
	},
	onopenclose:function(header, index, state, isuseractivated){ //custom code to run whenever a header is opened or closed
		//do nothing
	}
})
</script>

<link href="../resources/styles/highslide.css" type="text/css" rel="stylesheet" media="all" />   
<script type="text/javascript" src="../resources/scripts/highslide.js"></script>  

<script type="text/javascript">
	hs.graphicsDir = 'img/graphics/';
	hs.wrapperClassName = 'wide-border';
</script>

</head>

<body>
<div class="mainWrapper">
	<div class="mainHeader">
		<?php include("../includes/header_inner.php"); ?>
    </div>
    
    <div class="contAreaMainWrapper">
    
    <div class="innerPagesMainDiv">
   	  <div class="breadcrumbDiv">
        You are here :&nbsp;<span>Home</span> <span>Custom Product Development</span> Custom Rubber Formulation
      </div>
        
        <div class="productsPageMaindiv">
       	  <div class="productsLeftNavMain">
          
          <div class="glossymenu">
            <!--<a class="menuitem productsLeftListSelect" href="index.php">Custom Rubber Formulation</a>-->
            <a class="menuitem" href="rpd.php">Rubber Products Development</a>
            <a class="menuitem" href="emrp.php">Extruded and Molded Rubber Products</a>
            </div>
          	
          </div>
            
            <div class="productsRightMain">
           	  <div class="productDescTopMain">
                
                <div class="productDescTopRight" style="width:770px; padding:0px;">
                
                <div class="insideTopImage"><img src="../resources/images/custom_products_development.jpg" width="770" height="260" alt="" /></div>
                
               	  <h2>Custom Rubber Formulation</h2>
                  
                  <h3><div class="keyAdvantagesTopic" style="width:220px;">COMPONENT DESIGN ASSISTANCE</div></h3>
                  <ul class="keyAdvantages keyAdvantages_two">
                   	<li>We can accept Solid Works, iges, dfx and dwg files.</li>
                    <li>We can also work off prints and sketches faxed to us at +94-11-2532314</li>
                    <!--<li>Link to Designing with Rubber, Design Specification Checklist</li>-->
                    </ul>
                    
                   
                  <h3><div class="keyAdvantagesTopic" style="width:360px;">MATERIALS & COMPOUND FORMULATION DEVELOPMENT</div></h3>
                  <ul class="keyAdvantages keyAdvantages_two">
                   	<li>Over 100 compound formulations</li>
                    <li>Plus custom design and development of other compound formulations to meet your precise requirements</li>
                    
                    </ul>  
                    
                  <h3><div class="keyAdvantagesTopic" style="width:150px;">PROTOTYPE SERVICES</div></h3>
                  <ul class="keyAdvantages keyAdvantages_two">
                   	<li>A unique service to assist customers with product development and selection of appropriate polymers, materials and rubber formulation</li>
                    <li>A quick, economical way to test designs and ideas in actual working environments – before investing in production molds and tooling</li>
                    <li>Easy, quick, inexpensive modification of molds and tooling to ensure rubber component design is production ready</li>
                    <li>Completed in 21 to 30 working days depending on part complexity</li>
                    </ul> 
                    
                   <h3><div class="keyAdvantagesTopic" style="width:240px;">MULTIPLE COMPONENT ASSEMBLIES</div></h3>
                  	<ul class="keyAdvantages keyAdvantages_two">
                   	<li>Value added assemblies as simple as inserting a washer or disk into a rubber component nest</li>
                    <li>Or as complex as multiple rubber, plastic and metal components assembled, packaged and ready for product installation – straight from the box</li>
                    </ul>  
                    
                   <h3><div class="keyAdvantagesTopic" style="width:140px;">SPECIAL PACKAGING</div></h3>
                  	<ul class="keyAdvantages keyAdvantages_two">
                   	<li>Customer specific or bulk packing</li>
                    <li>Packaging for wholesale or retail stocking also could be arranged</li>
                    </ul> 
                                      
                   <h3><div class="keyAdvantagesTopic" style="width:260px;">PERFORMANCE TESTING & EVALUATION</div></h3>
                  	<ul class="keyAdvantages keyAdvantages_two">
                   	<li>•	In the prototype or product development stage</li>
                    <li>•	Or ongoing performance evaluation as part of the normal rubber component manufacturing</li>
                    </ul>  
                                       
                </div>
              </div>
              
              
              
            </div>
            
            
        
        </div>		
            
                        
        
    </div>     
        
  </div>
        
    <?php include("../includes/footer.php"); ?>  
        
    
</div>




</body>
</html>