<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="keywords" content="Rubber, rubber products, rubber bands, pallet & mover bands, Straps,  sri lankan rubber manufacturer "/>
<meta name="description" content="JB Rubber Products was incorporated in the early 80's to manufacture extruded rubber products such as rubber bands and has since evolved into a dynamic manufacturer of both extruded and moulded products.">
<meta name="author" content="">
<link rel="icon" href="../resources/images/favicon.ico">

<title>JAF RUBBER | Rubber Products Manufacturer - Sri Lanka | CUSTOM PRODUCT DEVELOPMENT | EXTRUDED AND MOLDED RUBBER PRODUCTS</title>

<link href="../resources/styles/main.css" type="text/css" rel="stylesheet" media="screen" />

<script type="text/javascript" src="../resources/scripts/jquery-1.7.1.min.js"></script>
<link rel="stylesheet" type="text/css" href="../resources/styles/ddsmoothmenu.css" />

<script type="text/javascript" src="../resources/scripts/ddsmoothmenu.js">
/***********************************************
* Smooth Navigational Menu- (c) Dynamic Drive DHTML code library (www.dynamicdrive.com)
* This notice MUST stay intact for legal use
* Visit Dynamic Drive at http://www.dynamicdrive.com/ for full source code
***********************************************/
</script>
<script type="text/javascript">
ddsmoothmenu.init({
	mainmenuid: "smoothmenu1", //menu DIV id
	orientation: 'h', //Horizontal or vertical menu: Set to "h" or "v"
	classname: 'ddsmoothmenu', //class added to menu's outer DIV
	//customtheme: ["#1c5a80", "#18374a"],
	contentsource: "markup" //"markup" or ["container_id", "path_to_menu_file"]
})
</script>

<link rel="stylesheet" type="text/css" href="../resources/styles/products_rightmenu.css" />
<script type="text/javascript" src="../resources/scripts/ddaccordion.js">
/***********************************************
* Accordion Content script- (c) Dynamic Drive DHTML code library (www.dynamicdrive.com)
* Visit http://www.dynamicDrive.com for hundreds of DHTML scripts
* This notice must stay intact for legal use
***********************************************/
</script>
<script type="text/javascript">
ddaccordion.init({
	headerclass: "submenuheader", //Shared CSS class name of headers group
	contentclass: "submenu", //Shared CSS class name of contents group
	revealtype: "click", //Reveal content when user clicks or onmouseover the header? Valid value: "click", "clickgo", or "mouseover"
	mouseoverdelay: 200, //if revealtype="mouseover", set delay in milliseconds before header expands onMouseover
	collapseprev: true, //Collapse previous content (so only one open at any time)? true/false 
	defaultexpanded: [], //index of content(s) open by default [index1, index2, etc] [] denotes no content
	onemustopen: false, //Specify whether at least one header should be open always (so never all headers closed)
	animatedefault: false, //Should contents open by default be animated into view?
	persiststate: false, //persist state of opened contents within browser session?
	toggleclass: ["", ""], //Two CSS classes to be applied to the header when it's collapsed and expanded, respectively ["class1", "class2"]
	togglehtml: ["suffix", "<img src='../resources/images/plus.gif' class='statusicon' />", "<img src='../resources/images/minus.gif' class='statusicon' />"], //Additional HTML added to the header when it's collapsed and expanded, respectively  ["position", "html1", "html2"] (see docs)
	animatespeed: "fast", //speed of animation: integer in milliseconds (ie: 200), or keywords "fast", "normal", or "slow"
	oninit:function(headers, expandedindices){ //custom code to run when headers have initalized
		//do nothing
	},
	onopenclose:function(header, index, state, isuseractivated){ //custom code to run whenever a header is opened or closed
		//do nothing
	}
})
</script>

<link href="../resources/styles/highslide.css" type="text/css" rel="stylesheet" media="all" />   
<script type="text/javascript" src="../resources/scripts/highslide.js"></script>  

<script type="text/javascript">
	hs.graphicsDir = 'img/graphics/';
	hs.wrapperClassName = 'wide-border';
</script>

</head>

<body>
<div class="mainWrapper">
	<div class="mainHeader">
		<?php include("../includes/header_inner.php"); ?>
    </div>
    
    <div class="contAreaMainWrapper">
    
    <div class="innerPagesMainDiv">
   	  <div class="breadcrumbDiv">
        You are here :&nbsp;<span>Home</span> <span>Custom Product Development</span> Extruded and Molded Rubber Products
      </div>
        
        <div class="productsPageMaindiv">
       	  <div class="productsLeftNavMain">
          
          <div class="glossymenu">
            <!--<a class="menuitem" href="index.php">Custom Rubber Formulation</a>-->
            <a class="menuitem" href="rpd.php">Rubber Products Development</a>
            <a class="menuitem productsLeftListSelect" href="emrp.php">Extruded and Molded Rubber Products</a>
            </div>
          	
          </div>
            
            <div class="productsRightMain">
           	  <div class="productDescTopMain">
                
                <div class="productDescTopRight" style="width:770px; padding:0px;">
                
                <div class="insideTopImage"><img src="../resources/images/emrp.jpg" width="770" height="260" alt="" /></div>
                
               	  <h2>Extruded and Molded Rubber Products</h2>
                  
                  <p>JB Rubber provides a variety of custom extruded and molded rubber products including rubber gaskets, seals, bumpers and a variety of other applications. We can supply custom extruded profiles in a variety of shapes and lengths with a rubber compound specifically designed to meet your requirements. We supply prototype extrusions as well as high volume production.</p>
                  <p>The extrusion process is accomplished by heating and shearing uncured rubber through the use of a temperature controlled auger screw, which pressurizes and forces the softened rubber through an extrusion die at the auger's output in a continuous fashion. A profiling extrusion die is fixed to the auger's outlet and the softened rubber takes on the shape of the die as it flows through it, resulting in a continuous strip of the desired profile. A variety of post extrusion curing can be done, depending on type of polymer.</p>

                  <h2>Extruded Rubber Profiles</h2>
                  
                  <p>With our in-house formulation and mixing capabilities, we can design a rubber compound to meet your specific requirements for quality and performance.</p>
                  
                  <h3><div class="keyAdvantagesTopic" style="width:200px;">EXTRUDED PROFILE POLYMERS</div></h3>
                  	
                    <ul class="keyAdvantages keyAdvantages_two">
                        <li>CR (Neoprene®)</li>
                        <li>EPDM (ethylene propylene)</li>
                        <li>IIR (butyl)</li>
                        <li>NBR (nitrile/buna n)</li>
                        <li>NR (natural rubber)</li>
                        <li>SBR (styrene butadiene)</li>
					</ul>
                    
                   <h3><div class="keyAdvantagesTopic" style="width:200px;">SPECIAL CAPABILITIES</div></h3>
                  	<ul class="keyAdvantages keyAdvantages_two">
                        <li>Microwave and autoclave (steam) vulcanization</li>
                        <li>Cut-to-length and continuous length extruded profiles</li>
                        <li>External identification printing</li>
					</ul>
                    
                    <h3><div class="keyAdvantagesTopic" style="width:200px;">SPECIFICATIONS</div></h3>
                  	<ul class="keyAdvantages keyAdvantages_two">
                        <li>Extruded profile sizes range from +.250" to +2.500"</li>
                        <li>Extruded profile wall thickness minimum .060"</li>
                        <li>External identification printing</li>
					</ul>
                    
                    <h3><div class="keyAdvantagesTopic" style="width:200px;">EXTRUDED PROFILE SHAPES</div></h3>
                  	<ul class="keyAdvantages keyAdvantages_two">
                        <li>D-, E-, H- and U-Channels</li>
                        <li>U-Channels with Bulbs</li>
                        <li>I-, L-, P- and T-Shaped Strips</li>
                        <li>Full Round Cord</li>
                        <li>Half Round Cord</li>
                        <li>Square Strips</li>
                        <li>Rectangular Strips</li>
                        <li>Mandrel Shaped Hoses, Elbows and Assemblies</li>
					</ul>
                    
                    <h2>Molded Rubber Components-Bonded-To-Substrates</h2>
                    
                    <p>JB Rubber Products has the specialized expertise to bond molded rubber components to many materials for whatever application you require. This includes multiple substrates and other combinations. Whatever your specifications, we can design a high-quality solution that performs best for you.</p>
                    
                    <ul class="keyAdvantages keyAdvantages_two">
                        <li>In-house compound formulation development and mixing capabilities.
                        <li>Over 100 formulations developed.</li>
                        <li>20 engineering grade polymers.</li>
                        <li>Wide variety of compound characteristics.</li>
                        <li>A range of specifications and properties.</li>
					</ul>
                    
                  <h2>Rubber-To-Substrate Materials</h2>
                  
                  <ul class="keyAdvantages keyAdvantages_two">
                        <li>Aluminum</li>
                        <li>Brass</li>
                        <li>Bronze</li>
                        <li>Carbon Steel</li>
                        <li>Ductile Iron</li>
                        <li>Fabric Reinforcement</li>
                        <li>Glass-Filled Composites</li>
                        <li>Mineral-Filled Composites</li>
                        <li>Nylon</li>
                        <li>Stainless Steel</li>
					</ul>
                    
                   <h2>Multiple Substrate Material Combination Molding</h2>
                   
                   <ul class="keyAdvantages keyAdvantages_two">
                        <li>Rubber-to-metal-to-plastic-combinations</li>
                    	<li>Valve and Control Assemblies</li>
                    	<li>Vibration Isolators</li>
                    	<li>Vibration Mounts</li>

					</ul>
                    
                                  
                <div class="floatLeft">
                <img src="../resources/images/rubberised_tank.jpg" width="204" height="266" alt="" />
                </div>
                                       
                </div>
              </div>
              
              
              
            </div>
            
            
        
        </div>		
            
                        
        
    </div>     
        
  </div>
        
    <?php include("../includes/footer.php"); ?>  
        
    
</div>




</body>
</html>