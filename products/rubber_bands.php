<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="keywords" content="Rubber, rubber products, rubber bands, pallet & mover bands, Straps,  sri lankan rubber manufacturer "/>
<meta name="description" content="JB Rubber Products was incorporated in the early 80's to manufacture extruded rubber products such as rubber bands and has since evolved into a dynamic manufacturer of both extruded and moulded products.">
<meta name="author" content="">
<link rel="icon" href="../resources/images/favicon.ico">

<title>JAF RUBBER | Rubber Products Manufacturer - Sri Lanka | PRODUCTS | RUBBER BANDS</title>

<link href="../resources/styles/main.css" type="text/css" rel="stylesheet" media="screen" />

<script type="text/javascript" src="../resources/scripts/jquery-1.7.1.min.js"></script>
<link rel="stylesheet" type="text/css" href="../resources/styles/ddsmoothmenu.css" />

<script type="text/javascript" src="../resources/scripts/ddsmoothmenu.js">
/***********************************************
* Smooth Navigational Menu- (c) Dynamic Drive DHTML code library (www.dynamicdrive.com)
* This notice MUST stay intact for legal use
* Visit Dynamic Drive at http://www.dynamicdrive.com/ for full source code
***********************************************/
</script>
<script type="text/javascript">
ddsmoothmenu.init({
	mainmenuid: "smoothmenu1", //menu DIV id
	orientation: 'h', //Horizontal or vertical menu: Set to "h" or "v"
	classname: 'ddsmoothmenu', //class added to menu's outer DIV
	//customtheme: ["#1c5a80", "#18374a"],
	contentsource: "markup" //"markup" or ["container_id", "path_to_menu_file"]
})
</script>

<link rel="stylesheet" type="text/css" href="../resources/styles/products_rightmenu.css" />
<script type="text/javascript" src="../resources/scripts/ddaccordion.js">
/***********************************************
* Accordion Content script- (c) Dynamic Drive DHTML code library (www.dynamicdrive.com)
* Visit http://www.dynamicDrive.com for hundreds of DHTML scripts
* This notice must stay intact for legal use
***********************************************/
</script>
<script type="text/javascript">
ddaccordion.init({
	headerclass: "submenuheader", //Shared CSS class name of headers group
	contentclass: "submenu", //Shared CSS class name of contents group
	revealtype: "mouseover", //Reveal content when user clicks or onmouseover the header? Valid value: "click", "clickgo", or "mouseover"
	mouseoverdelay: 300, //if revealtype="mouseover", set delay in milliseconds before header expands onMouseover
	collapseprev: true, //Collapse previous content (so only one open at any time)? true/false 
	defaultexpanded: [], //index of content(s) open by default [index1, index2, etc] [] denotes no content
	onemustopen: false, //Specify whether at least one header should be open always (so never all headers closed)
	animatedefault: false, //Should contents open by default be animated into view?
	persiststate: true, //persist state of opened contents within browser session?
	toggleclass: ["", ""], //Two CSS classes to be applied to the header when it's collapsed and expanded, respectively ["class1", "class2"]
	togglehtml: ["suffix", "<img src='../resources/images/plus.gif' class='statusicon' />", "<img src='../resources/images/minus.gif' class='statusicon' />"], //Additional HTML added to the header when it's collapsed and expanded, respectively  ["position", "html1", "html2"] (see docs)
	animatespeed: "fast", //speed of animation: integer in milliseconds (ie: 200), or keywords "fast", "normal", or "slow"
	oninit:function(headers, expandedindices){ //custom code to run when headers have initalized
		//do nothing
	},
	onopenclose:function(header, index, state, isuseractivated){ //custom code to run whenever a header is opened or closed
		//do nothing
	}
})
</script>

<script type="text/javascript" src="../resources/scripts/image-rotating.js"></script>
<script type="text/javascript">
$(document).ready(function() {
    $('.slideshow').cycle({
		fx: 'fade' // choose your transition type, ex: fade, scrollUp, shuffle, etc...
	});
});
</script>

</head>

<body>
<div class="mainWrapper">
	<div class="mainHeader">
		<?php include("../includes/header_inner.php"); ?>
    </div>
    
    <div class="contAreaMainWrapper">
    
    <div class="innerPagesMainDiv">
   	  <div class="breadcrumbDiv">
        You are here :&nbsp; <span>Products</span> Rubber Bands
      </div>
        
        <div class="productsPageMaindiv">
       	  <div class="productsLeftNavMain">
          
          <?php include("../includes/products_list.php"); ?>
          	
          </div>
            
            <div class="productsRightMain">
           	  <div class="productDescTopMain">
                <div class="productDescMainImage slideshow">
                <img src="../resources/images/rubber_bands_main.jpg" width="442" height="402" alt="" />
                <img src="../resources/images/rubber_bands_main2.jpg" width="442" height="402" alt="" />
                <img src="../resources/images/rubber_bands_main3.jpg" width="442" height="402" alt="" />
                <img src="../resources/images/rubber_bands_main4.jpg" width="442" height="402" alt="" />
                </div>
                <div class="productDescTopRight">
               	  <h2>Rubber Bands</h2>
                  <p>All bands are manufactured from high quality natural crepe rubber, to international standards in application specific compounds, colours and in a range of sizes.</p>
                    
                    <h3><div class="keyAdvantagesTopic">KEY ADVANTAGES</div></h3>
                    
                  <ul class="keyAdvantages">
                   	<li>High tensile strength</li>
                    <li>Superior elongation</li>
                    <li>Excellent stretch recovery</li>
                    <li>Resistance to outdoor use</li>
                    <li>High count/yield per lb.</li>
                    <li>Non-toxic</li>
                    </ul>
                    
                    <a href="../resources/docs/rubber_bands(low).pdf" target="_blank" class="download_broc"><img src="../resources/images/download.png" width="12" height="10" alt="" />&nbsp;&nbsp; DOWNLOAD BROCHURE</a>
                    
                    <a href="../resources/docs/rubber_bands_size_chart.pdf" target="_blank" class="download_broc_w"><img src="../resources/images/download_w.png" width="12" height="10" alt="" />&nbsp;&nbsp; SIZE CHART</a>
                    
                </div>
              </div>
              
              <div class="productsTypesMain">
              	<h2><div class="productsTypesTopics">TYPES</div></h2>
                
                <div class="productsTypesSet">
                <div class="floatLeft">
                <a href="rb_stationary_bands.php"><img src="../resources/images/pt_stationary_bands.jpg" width="180" height="120" alt="" />
                <!--<div class="productsTypesPopMain">
                   		<div class="floatLeft"><img src="../resources/images/pt_stationary_bands.jpg" width="180" height="120" alt="" /></div>
                        <div class="prodctTypesPopDesc">
                        	<h3>Stationary Bands</h3>
                            <p>This band is designed to withstand the sun for at least 10 -15 days in the field.  The leaves of the flower are banded together during the growing stages to protect the sun from bleaching the flower. Colors are used to indicate how long the cauliflower has been banded.</p>
                        </div>
                   </div>-->
                </a></div>
                    <h2><a href="rb_stationary_bands.php">Stationary Bands</a></h2>
              	</div>
                
                <div class="productsTypesSet">
               	   <div class="floatLeft"><a href="rb_agri_bands.php"><img src="../resources/images/pt_veg_bands.jpg" width="180" height="120" alt="" />
                   <!--<div class="productsTypesPopMain">
                   		<div class="floatLeft"><img src="../resources/images/pt_veg_bands.jpg" width="180" height="120" alt="" /></div>
                        <div class="prodctTypesPopDesc">
                        	<h3>Agricultural Bands</h3>
                            <p>JB produce Food Grade Quality rubber bands in natural crepe or our colored compound bands with a special cut for added strength. We supply Printed rubber Bands imprinted with logos, names, catch-phrases, code numbers or patterns for promotional uses & identification.</p>
                        </div>
                   </div>-->
                   </a>
 				</div>
                    <h2><a href="rb_agri_bands.php">Agricultural Bands</a></h2>
                    
              	</div>
                
                
             
                
                
                <!--<div class="productsTypesSet">
               	  
<div class="floatLeft"><a href="#"><img src="../resources/images/pt_agri_bands.jpg" width="180" height="120" alt="" />
				<div class="productsTypesPopMain">
                   		<div class="floatLeft"><img src="../resources/images/pt_agri_bands.jpg" width="180" height="120" alt="" /></div>
                        <div class="prodctTypesPopDesc">
                        	<h3>Agricultural Bands</h3>
                            <p>This band is designed to withstand the sun for at least 10 -15 days in the field.  The leaves of the flower are banded together during the growing stages to protect the sun from bleaching the flower. Colors are used to indicate how long the cauliflower has been banded.</p>
                        </div>
                   </div>
				</a></div>
                    <h2><a href="#">Agricultural Bands</a></h2>
              	</div>-->
                
                <div class="productsTypesSet">
               	  
				<div class="floatLeft"><a href="rb_postal_bands.php"><img src="../resources/images/pt_postal_bands.jpg" width="180" height="120" alt="" />
					<!--<div class="productsTypesPopMain productsTypesPopMain_corner">
                   		<div class="prodctTypesPopDesc" style="padding:0px 10px 0px 0px;">
                        	<h3>Postal Bands</h3>
                            <p>This band is designed to withstand the sun for at least 10 -15 days in the field.  The leaves of the flower are banded together during the growing stages to protect the sun from bleaching the flower. Colors are used to indicate how long the cauliflower has been banded.</p>
                        </div>
                        <div class="floatLeft"><img src="../resources/images/pt_postal_bands.jpg" width="180" height="120" alt="" /></div>
                   </div>-->
				</a></div>
                    <h2><a href="rb_postal_bands.phps">Postal Bands</a></h2>
              	</div>
                
                <div class="productsTypesSet" style="margin:0px;">
               	  
				<div class="floatLeft"><a href="rb_exercise_bands.php"><img src="../resources/images/pt_excercise_bands.jpg" width="180" height="120" alt="" />
                <!--<div class="productsTypesPopMain">
                   		<div class="floatLeft"><img src="../resources/images/pt_excercise_bands.jpg" width="180" height="120" alt="" /></div>
                        <div class="prodctTypesPopDesc">
                        	<h3>Exercise Bands</h3>
                            <p>This band is designed to withstand the sun for at least 10 -15 days in the field.  The leaves of the flower are banded together during the growing stages to protect the sun from bleaching the flower. Colors are used to indicate how long the cauliflower has been banded.</p>
                        </div>
                   </div>-->
                   </a></div>
                    <h2><a href="#">Exercise Bands</a></h2>
              	</div>
                
                <div class="productsTypesSet">
               	  
<div class="floatLeft"><a href="rb_antistatic_bands.php"><img src="../resources/images/pt_antistatic_bands.jpg" width="180" height="120" alt="" />
				<!--<div class="productsTypesPopMain">
                   		<div class="floatLeft"><img src="../resources/images/pt_antistatic_bands.jpg" width="180" height="120" alt="" /></div>
                        <div class="prodctTypesPopDesc">
                        	<h3>Anti Static Bands</h3>
                            <p>This band is designed to withstand the sun for at least 10 -15 days in the field.  The leaves of the flower are banded together during the growing stages to protect the sun from bleaching the flower. Colors are used to indicate how long the cauliflower has been banded.</p>
                        </div>
                   </div>-->
                </a></div>
                    <h2><a href="rb_antistatic_bands.php">Anti Static Bands</a></h2>
              	</div>
                
                <div class="productsTypesSet">
               	  
<div class="floatLeft"><a href="rb_heat_resistance_bands.php"><img src="../resources/images/pt_heatres_bands.jpg" width="180" height="120" alt="" />
				<!--<div class="productsTypesPopMain">
                   		<div class="floatLeft"><img src="../resources/images/pt_heatres_bands.jpg" width="180" height="120" alt="" /></div>
                        <div class="prodctTypesPopDesc">
                        	<h3>EPDM / Heat Resistant Bands</h3>
                            <p>This band is designed to withstand the sun for at least 10 -15 days in the field.  The leaves of the flower are banded together during the growing stages to protect the sun from bleaching the flower. Colors are used to indicate how long the cauliflower has been banded.</p>
                        </div>
                   </div>-->
                </a></div>
                    <h2><a href="rb_heat_resistance_bands.php">EPDM / Heat Resistant Bands</a></h2>
              	</div>
                
                <div class="productsTypesSet">
               	  
					<div class="floatLeft"><a href="rb_seamless_bands.php"><img src="../resources/images/pt_seamless18_bands.jpg" width="180" height="120" alt="" />
					<!--<div class="productsTypesPopMain productsTypesPopMain_corner">
                   		<div class="prodctTypesPopDesc" style="padding:0px 10px 0px 0px;">
                        	<h3>Seamless - up to 18” Bands</h3>
                            <p>This band is designed to withstand the sun for at least 10 -15 days in the field.  The leaves of the flower are banded together during the growing stages to protect the sun from bleaching the flower. Colors are used to indicate how long the cauliflower has been banded.</p>
                        </div>
                        <div class="floatLeft"><img src="../resources/images/pt_seamless18_bands.jpg" width="180" height="120" alt="" /></div>
                   </div>-->
					</a></div>
                    <h2><a href="rb_seamless_bands.php">Seamless - up to 18” Bands</a></h2>
              	</div>
                
                <div class="productsTypesSet"  style="margin:0px;">
                <div class="floatLeft">
                <a href="rb_tap_bands.php"><img src="../resources/images/pt_tab_bands.jpg" width="180" height="120" alt="" />
                <!--<div class="productsTypesPopMain">
                   		<div class="floatLeft"><img src="../resources/images/pt_tab_bands.jpg" width="180" height="120" alt="" /></div>
                        <div class="prodctTypesPopDesc">
                        	<h3>Tab / Arrow Bands</h3>
                            <p>&nbsp;</p>
                        </div>
                   </div>-->
                </a></div>
                    <h2><a href="rb_tap_bands.php">Tab / Arrow Bands</a></h2>
              	</div>

                <div class="productsTypesSet">
                  
<div class="floatLeft"><a href="rb_latex_bands.php"><img src="../resources/images/pt_latex_bands.jpg" width="180" height="120" alt="" />
        <!--<div class="productsTypesPopMain">
                      <div class="floatLeft"><img src="../resources/images/pt_antistatic_bands.jpg" width="180" height="120" alt="" /></div>
                        <div class="prodctTypesPopDesc">
                          <h3>Anti Static Bands</h3>
                            <p>This band is designed to withstand the sun for at least 10 -15 days in the field.  The leaves of the flower are banded together during the growing stages to protect the sun from bleaching the flower. Colors are used to indicate how long the cauliflower has been banded.</p>
                        </div>
                   </div>-->
                </a></div>
                    <h2><a href="rb_latex_bands.php">Latex-Free Bands</a></h2>
                </div>
                
                
              </div>
              
            </div>
            
            
        
        </div>		
            
                        
        
    </div>     
        
  </div>
        
    <?php include("../includes/footer.php"); ?>  
        
    
</div>




</body>
</html>