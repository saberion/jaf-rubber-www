<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="keywords" content="Rubber, rubber products, rubber bands, pallet & mover bands, Straps,  sri lankan rubber manufacturer "/>
<meta name="description" content="JB Rubber Products was incorporated in the early 80's to manufacture extruded rubber products such as rubber bands and has since evolved into a dynamic manufacturer of both extruded and moulded products.">
<meta name="author" content="">
<link rel="icon" href="../resources/images/favicon.ico">

<title>JAF RUBBER | Rubber Products Manufacturer - Sri Lanka | PRODUCTS | INDUSTRIAL PRODUCTS | RUBBER PROFILES</title>

<link href="../resources/styles/main.css" type="text/css" rel="stylesheet" media="screen" />

<script type="text/javascript" src="../resources/scripts/jquery-1.7.1.min.js"></script>
<link rel="stylesheet" type="text/css" href="../resources/styles/ddsmoothmenu.css" />

<script type="text/javascript" src="../resources/scripts/ddsmoothmenu.js">
/***********************************************
* Smooth Navigational Menu- (c) Dynamic Drive DHTML code library (www.dynamicdrive.com)
* This notice MUST stay intact for legal use
* Visit Dynamic Drive at http://www.dynamicdrive.com/ for full source code
***********************************************/
</script>
<script type="text/javascript">
ddsmoothmenu.init({
	mainmenuid: "smoothmenu1", //menu DIV id
	orientation: 'h', //Horizontal or vertical menu: Set to "h" or "v"
	classname: 'ddsmoothmenu', //class added to menu's outer DIV
	//customtheme: ["#1c5a80", "#18374a"],
	contentsource: "markup" //"markup" or ["container_id", "path_to_menu_file"]
})
</script>

<link rel="stylesheet" type="text/css" href="../resources/styles/products_rightmenu.css" />
<script type="text/javascript" src="../resources/scripts/ddaccordion.js">
/***********************************************
* Accordion Content script- (c) Dynamic Drive DHTML code library (www.dynamicdrive.com)
* Visit http://www.dynamicDrive.com for hundreds of DHTML scripts
* This notice must stay intact for legal use
***********************************************/
</script>
<script type="text/javascript">
ddaccordion.init({
	headerclass: "submenuheader", //Shared CSS class name of headers group
	contentclass: "submenu", //Shared CSS class name of contents group
	revealtype: "mouseover", //Reveal content when user clicks or onmouseover the header? Valid value: "click", "clickgo", or "mouseover"
	mouseoverdelay: 300, //if revealtype="mouseover", set delay in milliseconds before header expands onMouseover
	collapseprev: true, //Collapse previous content (so only one open at any time)? true/false 
	defaultexpanded: [], //index of content(s) open by default [index1, index2, etc] [] denotes no content
	onemustopen: false, //Specify whether at least one header should be open always (so never all headers closed)
	animatedefault: false, //Should contents open by default be animated into view?
	persiststate: true, //persist state of opened contents within browser session?
	toggleclass: ["", ""], //Two CSS classes to be applied to the header when it's collapsed and expanded, respectively ["class1", "class2"]
	togglehtml: ["suffix", "<img src='../resources/images/plus.gif' class='statusicon' />", "<img src='../resources/images/minus.gif' class='statusicon' />"], //Additional HTML added to the header when it's collapsed and expanded, respectively  ["position", "html1", "html2"] (see docs)
	animatespeed: "fast", //speed of animation: integer in milliseconds (ie: 200), or keywords "fast", "normal", or "slow"
	oninit:function(headers, expandedindices){ //custom code to run when headers have initalized
		//do nothing
	},
	onopenclose:function(header, index, state, isuseractivated){ //custom code to run whenever a header is opened or closed
		//do nothing
	}
})
</script>

</head>

<body>
<div class="mainWrapper">
	<div class="mainHeader">
		<?php include("../includes/header_inner.php"); ?>
    </div>
    
    <div class="contAreaMainWrapper">
    
    <div class="innerPagesMainDiv">
   	  <div class="breadcrumbDiv">
        You are here :&nbsp; <span>Products</span> <span>Industrial Products</span> Rubber Profiles
      </div>
        
        <div class="productsPageMaindiv">
       	  <div class="productsLeftNavMain">
          
          <?php include("../includes/products_list.php"); ?>
          
          </div>
            
            <div class="productsRightMain">
           	  <div class="productDescTopMain">
                <div class="productDescMainImage"><img src="../resources/images/rubber_profiles.jpg"  alt="" /></div>
                <div class="productDescTopRight">
               	  <h2>Rubber Profiles</h2>
                  <p>A recent expansion in April 2014 a state of the art continuous curing line was inaugurated to cater to the growing demand for extruded profiles from our customers. In-line with our mission to provide all our customers rubber products needs we have launched many new products. <br/><br/>

                  Our profiles are made from EPDM, NBR, SBR and NR compounds; they are well resistant to various mechanical, chemical and heat influences and suitable for a variety of applications.</p>
                  
                   
                    
                    <a href="../resources/docs/JB-Exports -Rubber-Profiles.pdf" target="_blank" class="download_broc"><img src="../resources/images/download.png" width="12" height="10" alt="" />&nbsp;&nbsp; DOWNLOAD BROCHURE</a>
                    
                </div>
              </div>
              
              <div class="productsTypesMain">
                <h2><div class="keyAdvantagesTopic">PRODCUT RANGE</div></h2>
                
                <div class="floatLeft">
                
                <div class="floatLeft" style="width:100%;">
                    <ul class="keyAdvantages keyAdvantages_two">
                        <li>Rubber profiles for Glass and Aluminum fabrications. Our sealing’s are designed for thermal insulation, dust and water sealing and combine with good aesthetic appearance.</li>
                        <li>Rubber profiles for construction and industrial freezers such as door sealing for thermal and damping application.</li>
                        <li>Rubber profiles for HVAC systems providing jointing seals and thermal insulation.</li>
                        <li>Sponge profiles for various application.</li>
                        <li>Custom profiles to meet many needs.</li>
                  </ul>
                </div>
                
                                
                </div>
                
              	<h2><div class="keyAdvantagesTopic">KEY ADVANTAGES</div></h2>
                
                <div class="floatLeft">
                
                <div class="floatLeft" style="width:100%;">
                    <ul class="keyAdvantages keyAdvantages_two">
                        <li>Rubber profiles for aluminum windows producers.</li>
                        <li>Rubber profiles for the building, car, electric power, white goods and other industries.</li>
                        <li>Sponge rubber profiles of various shapes and dimensions.</li>
                        <li>Different types of tailor-made profiles to meet customer requirements.</li>
                        <li>Our profiles are made from EPDM, CR, NBR, SBR and NR compounds; they are well resistant to various mechanical, chemical and heat influences, and suitable for a variety of applications.</li>
                  </ul>
                </div>
                
                                
                </div>
                
                
                
                <h2><div class="keyAdvantagesTopic" style="width:150px;">PRODUCT APPLICATION</div></h2>
                
                <div class="floatLeft">
                <img src="../resources/images/rub_prof_pa_01.jpg" width="163" height="122" alt="" style="padding-right:15px;" />
                <img src="../resources/images/rub_prof_pa_02.jpg" width="163" height="122" alt="" style="padding-right:15px;" />
                <img src="../resources/images/rub_prof_pa_03.jpg" width="163" height="122" alt="" style="padding-right:15px;" />
                <img src="../resources/images/rub_prof_pa_04.jpg" width="163" height="122" alt="" />
                
                </div>
                
                
              </div>  
                
              
              
            </div>
            
            
        
        </div>		
            
                        
        
    </div>     
        
  </div>
        
    <?php include("../includes/footer.php"); ?>  
        
    
</div>




</body>
</html>