<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="keywords" content="Rubber, rubber products, rubber bands, pallet & mover bands, Straps,  sri lankan rubber manufacturer "/>
<meta name="description" content="JB Rubber Products was incorporated in the early 80's to manufacture extruded rubber products such as rubber bands and has since evolved into a dynamic manufacturer of both extruded and moulded products.">
<meta name="author" content="">
<link rel="icon" href="../resources/images/favicon.ico">

<title>JAF RUBBER | Rubber Products Manufacturer - Sri Lanka | PRODUCTS | STRAPS | RUBBER ROPES</title>

<link href="../resources/styles/main.css" type="text/css" rel="stylesheet" media="screen" />

<script type="text/javascript" src="../resources/scripts/jquery-1.7.1.min.js"></script>
<link rel="stylesheet" type="text/css" href="../resources/styles/ddsmoothmenu.css" />

<script type="text/javascript" src="../resources/scripts/ddsmoothmenu.js">
/***********************************************
* Smooth Navigational Menu- (c) Dynamic Drive DHTML code library (www.dynamicdrive.com)
* This notice MUST stay intact for legal use
* Visit Dynamic Drive at http://www.dynamicdrive.com/ for full source code
***********************************************/
</script>
<script type="text/javascript">
ddsmoothmenu.init({
	mainmenuid: "smoothmenu1", //menu DIV id
	orientation: 'h', //Horizontal or vertical menu: Set to "h" or "v"
	classname: 'ddsmoothmenu', //class added to menu's outer DIV
	//customtheme: ["#1c5a80", "#18374a"],
	contentsource: "markup" //"markup" or ["container_id", "path_to_menu_file"]
})
</script>

<link rel="stylesheet" type="text/css" href="../resources/styles/products_rightmenu.css" />
<script type="text/javascript" src="../resources/scripts/ddaccordion.js">
/***********************************************
* Accordion Content script- (c) Dynamic Drive DHTML code library (www.dynamicdrive.com)
* Visit http://www.dynamicDrive.com for hundreds of DHTML scripts
* This notice must stay intact for legal use
***********************************************/
</script>
<script type="text/javascript">
ddaccordion.init({
	headerclass: "submenuheader", //Shared CSS class name of headers group
	contentclass: "submenu", //Shared CSS class name of contents group
	revealtype: "mouseover", //Reveal content when user clicks or onmouseover the header? Valid value: "click", "clickgo", or "mouseover"
	mouseoverdelay: 300, //if revealtype="mouseover", set delay in milliseconds before header expands onMouseover
	collapseprev: true, //Collapse previous content (so only one open at any time)? true/false 
	defaultexpanded: [], //index of content(s) open by default [index1, index2, etc] [] denotes no content
	onemustopen: false, //Specify whether at least one header should be open always (so never all headers closed)
	animatedefault: false, //Should contents open by default be animated into view?
	persiststate: true, //persist state of opened contents within browser session?
	toggleclass: ["", ""], //Two CSS classes to be applied to the header when it's collapsed and expanded, respectively ["class1", "class2"]
	togglehtml: ["suffix", "<img src='../resources/images/plus.gif' class='statusicon' />", "<img src='../resources/images/minus.gif' class='statusicon' />"], //Additional HTML added to the header when it's collapsed and expanded, respectively  ["position", "html1", "html2"] (see docs)
	animatespeed: "fast", //speed of animation: integer in milliseconds (ie: 200), or keywords "fast", "normal", or "slow"
	oninit:function(headers, expandedindices){ //custom code to run when headers have initalized
		//do nothing
	},
	onopenclose:function(header, index, state, isuseractivated){ //custom code to run whenever a header is opened or closed
		//do nothing
	}
})
</script>

</head>

<body>
<div class="mainWrapper">
	<div class="mainHeader">
		<?php include("../includes/header_inner.php"); ?>
    </div>
    
    <div class="contAreaMainWrapper">
    
    <div class="innerPagesMainDiv">
   	  <div class="breadcrumbDiv">
        You are here :&nbsp; <span>Products</span> <span>Industrial Products</span> Rubber Ropes
      </div>
        
        <div class="productsPageMaindiv">
       	  <div class="productsLeftNavMain">
          
          <?php include("../includes/products_list.php"); ?>
          
          </div>
            
            <div class="productsRightMain">
           	  <div class="productDescTopMain">
                <div class="productDescMainImage"><img src="../resources/images/rubber_ropes.jpg" alt="" /></div>
                <div class="productDescTopRight">
               	  <h2>Rubber Ropes</h2>
                  <p>Rubber rope is designed to complement the JB Tie down range is a versatile and flexible solution for large and irregular loads and application; typical in lumber, farming, garbage and mining applications.<br/><br/>
                    Rubber rope is designed to be coupled with J hooks to make custom length tie downs. The rubber ropes are offered in both Natural rubber and EPDM to in both solid and hollow profile options with diameter 3/8" and 7/16".
                  </p>

					                    
                                    
                    <a href="../resources/docs/JB_rubber_ropes.pdf" target="_blank" class="download_broc"><img src="../resources/images/download.png" width="12" height="10" alt="" />&nbsp;&nbsp; DOWNLOAD BROCHURE</a>
                    
                </div>
              </div>
              
              <div class="productsTypesMain">
              	<h2><div class="keyAdvantagesTopic">KEY ADVANTAGES</div></h2>
                                
                <div class="floatLeft" style="width:100%;">
                    <ul class="keyAdvantages keyAdvantages_two">
                        <li>Natural rubber offers high tear resistance, superior elongation and low temperature flexibility for the most demanding applications.</li>
                        <li>EPDM offers the same high strength and but extended weather resistance and ideal for applications with long term outdoor exposure.</li>
                  </ul>
                    
                </div>
                
                <h2><div class="keyAdvantagesTopic" style="width:120px;">PACKING OPTIONS</div></h2>
                                
                <div class="floatLeft" style="width:100%;">
                    <ul class="keyAdvantages keyAdvantages_two">
                        <li>Bulk packing up to 600 ft lengths</li>
                        <li>Packed in spools of 150 ft length & 4 spools to a carton.</li>
                        <li>Packed in carton with dispenser retail usage.</li>
                  </ul>
                    
                </div>
                
                <h2><div class="keyAdvantagesTopic" style="width:50px;">SIZES</div></h2>
                    
                  	 <table width="100%" border="0" cellspacing="0" cellpadding="0" class="keyAdvantagesTbl">
                      <tr>
                        <td><strong>Diameter</strong></td>
                        <td><strong>Type</strong></td>
                        <td><strong>Length</strong></td>
                        <td><strong>Coil per carton</strong></td>
                      </tr>
                      <tr>
                        <td>3/8" Diameter</td>
                        <td>Solid</td>
                        <td>150’-600’</td>
                        <td>1-4</td>
                      </tr>
                      <tr>
                        <td>3/8" Diameter</td>
                        <td>5 mm hole/Hollow core</td>
                        <td>100’-300’</td>
                        <td>1</td>
                      </tr>
                      <tr>
                        <td>7/16" Diameter</td>
                        <td>Solid</td>
                        <td>150’-600’</td>
                        <td>1-4</td>
                      </tr>
                    </table>
                	
                    <p>&nbsp;</p>
                    
                <h2><div class="keyAdvantagesTopic" style="width:130px;">PACKING METHODS</div></h2>
                                
                <div class="floatLeft" style="width:100%;">
                    <ul class="keyAdvantages keyAdvantages_two">
                        <li><a href="../resources/docs/rope-shrink-wrap-pack.pdf" target="_blank" class="download_broc_common"><img src="../resources/images/download_w.png" width="12" height="10" alt="" />&nbsp;&nbsp; BULK PACK -  DOWNLOAD BROCHURE</a></li>
                        <li><a href="../resources/docs/rope-individual-pack.pdf" target="_blank" class="download_broc_common"><img src="../resources/images/download_w.png" width="12" height="10" alt="" />&nbsp;&nbsp; INDIVIDUAL PACK -  DOWNLOAD BROCHURE</a></li>
                  </ul>
                    
                </div>
                
                                    
               	<!--<h2><div class="keyAdvantagesTopic" style="width:150px;">PRODUCT APPLICATION</div></h2>
                
                <div class="floatLeft">
                <img src="../resources/images/dolly_caps_pa.jpg" width="246" height="237" alt="" />
                </div>-->
                
              </div>
              
            </div>
            
            
        
        </div>		
            
                        
        
    </div>     
        
  </div>
        
    <?php include("../includes/footer.php"); ?>  
        
    
</div>




</body>
</html>