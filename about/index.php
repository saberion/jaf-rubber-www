<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="keywords" content="Rubber, rubber products, rubber bands, pallet & mover bands, Straps,  sri lankan rubber manufacturer "/>
<meta name="description" content="JB Rubber Products was incorporated in the early 80's to manufacture extruded rubber products such as rubber bands and has since evolved into a dynamic manufacturer of both extruded and moulded products.">
<meta name="author" content="">
<link rel="icon" href="../resources/images/favicon.ico">

<title>JAF RUBBER | Rubber Products Manufacturer - Sri Lanka | About Us</title>

<link href="../resources/styles/main.css" type="text/css" rel="stylesheet" media="screen" />

<script type="text/javascript" src="../resources/scripts/jquery-1.7.1.min.js"></script>
<link rel="stylesheet" type="text/css" href="../resources/styles/ddsmoothmenu.css" />

<script type="text/javascript" src="../resources/scripts/ddsmoothmenu.js">
/***********************************************
* Smooth Navigational Menu- (c) Dynamic Drive DHTML code library (www.dynamicdrive.com)
* This notice MUST stay intact for legal use
* Visit Dynamic Drive at http://www.dynamicdrive.com/ for full source code
***********************************************/
</script>
<script type="text/javascript">
ddsmoothmenu.init({
	mainmenuid: "smoothmenu1", //menu DIV id
	orientation: 'h', //Horizontal or vertical menu: Set to "h" or "v"
	classname: 'ddsmoothmenu', //class added to menu's outer DIV
	//customtheme: ["#1c5a80", "#18374a"],
	contentsource: "markup" //"markup" or ["container_id", "path_to_menu_file"]
})
</script>

<link rel="stylesheet" type="text/css" href="../resources/styles/products_rightmenu.css" />
<script type="text/javascript" src="../resources/scripts/ddaccordion.js">
/***********************************************
* Accordion Content script- (c) Dynamic Drive DHTML code library (www.dynamicdrive.com)
* Visit http://www.dynamicDrive.com for hundreds of DHTML scripts
* This notice must stay intact for legal use
***********************************************/
</script>
<script type="text/javascript">
ddaccordion.init({
	headerclass: "submenuheader", //Shared CSS class name of headers group
	contentclass: "submenu", //Shared CSS class name of contents group
	revealtype: "click", //Reveal content when user clicks or onmouseover the header? Valid value: "click", "clickgo", or "mouseover"
	mouseoverdelay: 200, //if revealtype="mouseover", set delay in milliseconds before header expands onMouseover
	collapseprev: true, //Collapse previous content (so only one open at any time)? true/false 
	defaultexpanded: [], //index of content(s) open by default [index1, index2, etc] [] denotes no content
	onemustopen: false, //Specify whether at least one header should be open always (so never all headers closed)
	animatedefault: false, //Should contents open by default be animated into view?
	persiststate: false, //persist state of opened contents within browser session?
	toggleclass: ["", ""], //Two CSS classes to be applied to the header when it's collapsed and expanded, respectively ["class1", "class2"]
	togglehtml: ["suffix", "<img src='../resources/images/plus.gif' class='statusicon' />", "<img src='../resources/images/minus.gif' class='statusicon' />"], //Additional HTML added to the header when it's collapsed and expanded, respectively  ["position", "html1", "html2"] (see docs)
	animatespeed: "fast", //speed of animation: integer in milliseconds (ie: 200), or keywords "fast", "normal", or "slow"
	oninit:function(headers, expandedindices){ //custom code to run when headers have initalized
		//do nothing
	},
	onopenclose:function(header, index, state, isuseractivated){ //custom code to run whenever a header is opened or closed
		//do nothing
	}
})
</script>

</head>

<body>
<div class="mainWrapper">
	<div class="mainHeader">
		<?php include("../includes/header_inner.php"); ?>
    </div>
    
    <div class="contAreaMainWrapper">
    
    <div class="innerPagesMainDiv">
   	  <div class="breadcrumbDiv">
        You are here :&nbsp; <span>Home</span> About Us
      </div>
        
        <div class="productsPageMaindiv">
       	  <div class="productsLeftNavMain">
          
          <div class="glossymenu">
            <a class="menuitem productsLeftListSelect" href="index.php">About Us</a>
            <a class="menuitem" href="#">News</a>
            <a class="menuitem" href="../contact/index.php">Contact Us</a>
            </div>
          	
          </div>
            
            <div class="productsRightMain">
           	  <div class="productDescTopMain">
                
                <div class="productDescTopRight" style="width:770px; padding:0px;">
                	<div class="insideTopImage"><img src="../resources/images/about_image.jpg" width="770" height="260" alt="" /></div>
                	<h2>Jafferjee Brothers Exports (Pvt) Ltd</h2>
                  	
                    <h3><div class="keyAdvantagesTopic" style="width:100px;">OUR COMPANY</div></h3>
                    
                  <p style="float:left;">JB Rubber Products was incorporated in the early 80's to manufacture extruded rubber products such as rubber bands and has since evolved into a dynamic manufacturer of both extruded and moulded products. The firm has a leadership position is providing industrial rubber bands solutions and rubber tie downs to the cargo control industries worldwide. In addition to this our technical product line is closely tied into the supply chains of leading OEM manufacturers providing them engineered technical products to meet entire spectrum of rubber product's needs.</p>
                  <p>Presently, the company owns and operates purpose build production facility of over 65,000sqft with a rated capacity to manufacture over 3,000 metric tons of rubber products and worldwide sales of over US $ 10 m per annum. Capabilities also extend to produce EDPM, SBR, Nitrile, Poly-isoprene and Neoprene, plastic-rubber, metal-rubber and range of coloured product options. In accordance with global quality standards our facilities are IS0 9001 certified by SGS UL.  Additionally, the company is also complaint with all global stands in social and ethical business practices.</p>
                  <p>The company's philosophy is to provide its customers cost-effective product solutions of superior quality with reliable delivery and professional service.  This customer-centric approach has been the cornerstone to establishing a diverse and loyal customer base spread across the globe. Currently the firm's key customers are in North America and Europe where it has gained a reputation as the preferred supplier.  The organization takes pride in the fact that the companies key customers are relationships built from the very inception of the firm and serve as a testament to this superior approach and strengthen the flagship JB-branded products which are undisputed market leaders and synonymous with high-performance and reliability! </p>
                    
                    <!--<h3><div class="keyAdvantagesTopic" style="width:80px;">OUR VISION</div></h3>
                    
                    <p style="float:left;">JB will advance the science of motion performance by being the partner of choice for the most trusted systems, services, and components in diverse applications.</p>-->
                    
                    <h3><div class="keyAdvantagesTopic" style="width:82px;">OUR VALUES</div></h3>
                    
                    <p style="float:left;"><strong>Integrity :</strong> We conduct all our business with honesty, integrity and professionalism in order to build lasting and long term business relationships and to create win-win partnership with all our stakeholders.</p>
                    
                                      
                  <h3><div class="keyAdvantagesTopic" style="width:100px;">OUR STRENGTH</div></h3>
                    
                  	<ul class="keyAdvantages keyAdvantages_two" style="padding-bottom:20px;">
                        <li> Dedicated, skilled and talented team</li> 
                        <li> Strong focus on customer needs of reliability, flexibility, sustainability and quality</li>
                        <li> Ability to handle diverse product range and market segments</li>
                        <li> Cost efficient operations. and strong continuous improvement framework to deliver value</li>
                        <li> History of consistent profitability and growth with strong asset base</li>
                  </ul>
                    
                    <!--<p><strong>Accountability : </strong> We are thoughtful in our planning, take ownership of our performance & accept responsibility for working safely.</p>
                    <p><strong>Initiative :</strong> We are always looking ahead; we follow through energetically and with determination; we strive for service excellence.</p>
                    <p><strong>Collaboration :</strong> We are willing and cooperative; our teamwork makes us more successful.</p>
                    <p><strong>Curiosity :</strong> Our thirst for knowledge helps us see opportunities; we continually seek new information.</p>
					<p><strong>Quality :</strong> What we do, we do well; we are uncompromising in the quality and safety of our systems,services,and products.</p>-->
                                        
                </div>
              </div>
              
              
              
            </div>
            
            
        
        </div>		
            
                        
        
    </div>     
        
  </div>
        
    <?php include("../includes/footer.php"); ?>  
        
    
</div>




</body>
</html>