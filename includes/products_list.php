
<div class="glossymenu">
<a class="menuitem" href="../products/rubber_bands.php">Rubber Bands</a>
<a class="menuitem" href="../products/pallets_mover_bands.php">Pallet & Mover Bands</a>
<a class="menuitem" href="../products/tie_down_straps.php">Tie Down Straps</a>
<a class="menuitem submenuheader" href="../products/straps_main.php" >Straps</a>
<div class="submenu">
    <ul>
    <li><a href="../products/truck_straps.php">Truck Straps</a></li>
    <li><a href="../products/roll_container_straps.php">Roll Container Straps</a></li>
    <li><a href="../products/rubber_ropes.php">Rubber Rope</a></li>
    <li><a href="../products/boat_mooring_absorbers.php">Boat Mooring Absorbers</a></li>
    <!--<li><a href="../products/ladder_strap.php">Ladder /Continues Strap</a></li>-->
    <li><a href="../products/meter_tensioner.php">Meter Tensioner</a></li>
    </ul>
</div>
<a class="menuitem submenuheader" href="../products/industrial_products.php">Industrial Products</a>
<div class="submenu">
    <ul>
    <li><a href="../products/industrial_components.php">Industrial Components</a></li>
    <li><a href="../products/lamp_holder.php">Lamp Holders</a></li>
    <li><a href="../products/dolly_caps.php">Dolly Caps</a></li>
    <li><a href="../products/rubber_profiles.php">Rubber Profiles</a></li>
    <li><a href="../products/rubber_stair_case.php">Rubber Staircase Strips</a></li>
    </ul>
</div>
<a class="menuitem submenuheader" href="../products/household_products.php">Household Products</a>
<div class="submenu">
    <ul>
    <li><a href="../products/door_wedges.php">Door Wedges</a></li>
    <li><a href="../products/door_stoppers.php">Door Stoppers</a></li>
    </ul>
</div>	
</div>