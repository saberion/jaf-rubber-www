    	<div class="mainLogo"><a href="index.php"><img src="resources/images/jaf-rubber-logo.jpg" width="184" height="62" alt="" /></a></div>
        <div class="headerRight">
        	<div class="headerRightTop">
            	<ul>
                	  <li><a href="contact/index.php" style="border:none; padding-right:1px;">Contact Us</a></li>
                    <li><a href="#">News</a></li>
                    <li><a href="http://www.jafferjeebrothers.com/" target="_blank">JB Group</a></li>
                    <li><a href="about/index.php">About Us</a></li>
                </ul>
            </div>
            <div class="headerRightBot">
            <div class="MainNavDiv">
                <div id="smoothmenu1" class="ddsmoothmenu">
                <ul>
                <li style="margin-right:18px;"><a href="#">PRODUCTS</a>
                    <ul>
                      <li><a href="products/rubber_bands.php">RUBBER BAND</a></li>
                      <li><a href="products/pallets_mover_bands.php">PALLET & MOVER BANDS</a></li>
                      <li><a href="products/tie_down_straps.php">TIE DOWN STRAP</a></li>
                      <li><a href="products/straps_main.php">STRAPS</a>
                      		<ul>
                            <li><a href="products/truck_straps.php">Truck Straps</a></li>
                            <li><a href="products/roll_container_straps.php">Roll Container Straps</a></li>
                            <li><a href="products/boat_mooring_absorbers.php">Boat Mooring Absorbers</a></li>
                            <!--<li><a href="products/ladder_strap.php">Ladder /Continues Strap</a></li>-->
                            <li><a href="products/meter_tensioner.php">Meter Tensioner</a></li>
                            </ul>
                      </li>
                      <li><a href="products/industrial_products.php">INDUSTRIAL PRODUCTS</a>
                      		<ul>
                            <li><a href="products/industrial_components.php">Industrial Components</a></li>
                            <li><a href="products/lamp_holder.php">Lamp Holders</a></li>
                            <li><a href="products/dolly_caps.php">Dolly Caps</a></li>
                            <li><a href="products/rubber_ropes.php">Rubber Rope</a></li>
                            <li><a href="products/rubber_profiles.php">Rubber Profiles</a></li>
                            <li><a href="products/rubber_stair_case.php">Rubber Staircase Strips</a></li>
                            </ul>
                      </li>
                      <li><a href="products/household_products.php">HOUSEHOLD PRODUCTS</a>
                      		<ul>
                            <li><a href="products/door_wedges.php">Door Wedges</a></li>
                            <li><a href="products/door_stoppers.php">Door Stoppers</a></li>
                            </ul>
                      </li>
                    </ul>
                </li>
                <!--<li style="margin-right:18px;"><a href="#">INDUSTRY</a>
                    <ul>
                      <li><a href="#">TRUCKING &amp; CARGO CONTROL</a></li>
                      <li><a href="#">MATERIAL HANDLING &amp; LOGISTICS</a></li>
                      <li><a href="#">MANUFACTURING</a></li>
                      <li><a href="#">AGRICULTURE &amp; COMMERCIAL</a></li>
                  </ul>
                </li>-->
                <li style="margin-right:18px;"><a href="quality/index.php">QUALITY</a>
                	<ul>
                      <li><a href="quality/index.php">QUALITY POLICY</a></li>
                      <li><a href="quality/iso.php">ISO 9001:2008 CERTIFICATE</a></li>
                      <li><a href="quality/environmental.php" >ENVIRONMENTAL POLICY</a></li>
                  </ul>
                </li>
                <li><a href="custom_pro/index.php">CUSTOM PRODUCT DEVELOPMENT</a>
                <ul>
                      <!--<li><a href="custom_pro/index.php" style="width:188px;">CUSTOM RUBBER FORMULATION</a></li>-->
                      <li><a href="custom_pro/rpd.php" style="width:188px;">RUBBER PRODUCTS DEVELOPMENT</a></li>
                      <li><a href="custom_pro/emrp.php" style="width:188px;" >EXTRUDED AND MOLDED RUBBER PRODUCTS</a></li>
                  </ul>
                </li>
                
                </ul>
                <br style="clear: left" />
                </div>
              </div>
              
              <div class="searchDiv">
              <form method="get" action="http://www.google.com/search">
              <input name="" type="button" class="searchBtn" />
              <input type="text"  class="searchArea"    name="q" size="25" maxlength="255" value="" />
            	</form>
              </div>
              
            </div>
            
        </div>