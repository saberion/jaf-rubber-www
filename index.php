<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="keywords" content="Rubber, rubber products, rubber bands, pallet & mover bands, Straps,  sri lankan rubber manufacturer "/>
<meta name="description" content="JB Rubber Products was incorporated in the early 80's to manufacture extruded rubber products such as rubber bands and has since evolved into a dynamic manufacturer of both extruded and moulded products.">
<meta name="author" content="">
<link rel="icon" href="resources/images/favicon.ico">

<title>JAF RUBBER | Rubber Products Manufacturer - Sri Lanka | HOME</title>
<link href="resources/styles/main.css" type="text/css" rel="stylesheet" media="screen" />

<script type="text/javascript" src="resources/scripts/jquery-1.7.1.min.js"></script>
<link rel="stylesheet" type="text/css" href="resources/styles/ddsmoothmenu.css" />

<script type="text/javascript" src="resources/scripts/ddsmoothmenu_home.js">
/***********************************************
* Smooth Navigational Menu- (c) Dynamic Drive DHTML code library (www.dynamicdrive.com)
* This notice MUST stay intact for legal use
* Visit Dynamic Drive at http://www.dynamicdrive.com/ for full source code
***********************************************/
</script>
<script type="text/javascript">
ddsmoothmenu.init({
	mainmenuid: "smoothmenu1", //menu DIV id
	orientation: 'h', //Horizontal or vertical menu: Set to "h" or "v"
	classname: 'ddsmoothmenu', //class added to menu's outer DIV
	//customtheme: ["#1c5a80", "#18374a"],
	contentsource: "markup" //"markup" or ["container_id", "path_to_menu_file"]
})
</script>

<link rel="stylesheet" type="text/css" href="resources/styles/main_images_hover.css" />
<link rel="stylesheet" href="resources/styles/mosaic.css" type="text/css" media="screen" />
<script type="text/javascript" src="resources/scripts/mosaic.1.0.1.js"></script>	
<script type="text/javascript">  
	jQuery(function($){
		
		$('.bar2').mosaic({
			animation	:	'slide'//fade or slide
		});
		
	$('.js-link').click(function() {
		window.location = $(this).data('url');
	});
    
	});
</script>

<script type="text/javascript" src="resources/scripts/jquery.nivo.slider.js"></script>
<script type="text/javascript">
$(window).load(function() {
	$('#slider').nivoSlider();
});
</script> 
<link rel="stylesheet" href="resources/styles/nivo-slider.css" type="text/css" media="screen" />

</head>

<body>

<div class="mainWrapper">
	<div class="mainHeader">
		<?php include("includes/header_home.php"); ?>
    </div>
    
    <div class="contAreaMainWrapper">
        	<div class="homeImageAreaMain">
            	
                <div class="photo">
                    
                    <ul class="topic" id="menu">
                        
                        <li>
                        <div class="slideUpSet1">
                        <div class="mosaic-block bar2">
                        <a class="set photoOne mosaic-overlay" href="#Portraits">
                        	<h2>Trucking & Cargo Control<br /> &nbsp;</h2>
                        	<div class="details">
                                <span class="js-link" data-url="products/truck_straps.php">Trap Straps</span>
                                <span class="js-link" data-url="products/meter_tensioner.php">Meter Tensioner</span>
                                <span class="js-link" data-url="products/roll_container_straps.php">Roll Container Straps</span>
                                <span class="js-link" data-url="products/boat_mooring_absorbers.php">Boat Mooring Absorbers</span>
                                
                            </div></a>
                            
                        </div>
                        </div>
                        <ul>
                                <li><img src="resources/images/main_image01.jpg" width="1000" height="500" alt="" /></li>
                                
                            </ul>
                        </li>
                    
                        <li>
                        	<div class="slideUpSet2">
                        <div class="mosaic-block bar2">
                        <a class="set photoTwo mosaic-overlay" href="#Portraits">
                        	<h2>Material Handling & Logistics</h2>
                        	<div class="details">
                                <span class="js-link" data-url="products/pallets_mover_bands.php">Pallet & Mover Bands</span>
                                <!--<span class="js-link" data-url="products/pallets_mover_bands.php">Mover Bands</span>-->
                                <span class="js-link" data-url="products/rubber_ropes.php">Rubber Rope</span>
                                <span class="js-link" data-url="products/dolly_caps.php">Dolly Caps</span>
                                
                            </div></a>
                            
                        </div>
                        </div>
                        
                            <ul>
                                <li><img src="resources/images/main_image02.jpg" width="1000" height="500" alt="" /></li>
                                
                            </ul>
                        
                        </li>
                        <li>
                        	<div class="slideUpSet3">
                        <div class="mosaic-block bar2">
                        <a class="set photoThree mosaic-overlay" href="#Portraits">
                        	<h2>Manufacturing<br /> &nbsp;</h2>
                        	<div class="details">
                                <!--<span class="js-link" data-url="products/truck_straps.php">Washing Machines</span>
                                <span class="js-link" data-url="products/door_wedges.php">Door Wedges</span>
                                <span class="js-link" data-url="products/door_stoppers.php">Door Stoppers</span>-->
                                <span class="js-link" data-url="products/industrial_components.php">Industrial Components</span>
                                <span class="js-link" data-url="products/lamp_holder.php">Lamp Holder</span>
                                <span class="js-link" data-url="products/rubber_profiles.php">Rubber Profiles</span>
                                <span class="js-link" data-url="products/rubber_stair_case.php">Rubber Staircase Strips</span>
                                
                            </div></a>
                            
                        </div>
                        </div>
                        
                            <ul>
                               <li><img src="resources/images/main_image03.jpg" width="1000" height="500" alt="" /></li>
                                
                            </ul>
                        
                        </li>
                        <li>
                        	<div class="slideUpSet4">
                        <div class="mosaic-block bar2">
                        <a class="set photoFour mosaic-overlay" href="#Portraits">
                        	<h2>Agriculture &amp; Commercial<br /> &nbsp;</h2>
                        	<div class="details">
                                <span class="js-link" data-url="products/products/rb_tap_bands.php">Tab / Arrow Bands</span>
                                <span class="js-link" data-url="products/rb_seamless_bands.php">Seamless Bands</span>
                                <span class="js-link" data-url="products/rb_agri_lobster_bands.php">Lobster Bands</span>
                                <!--<span class="js-link" data-url="products/truck_straps.php">Fern Bands</span>-->
                                
                            </div></a>
                            
                        </div>
                        </div>
                            <ul>
                                <li><img src="resources/images/main_image04.jpg" width="1000" height="500" alt="" /></li>
                                
                            </ul>
                        
                        </li>
                        
                    </ul>
<br class="clear" />
</div>
                        
            <div class="mainImageContiSlider"> 
             <div id="slider" class="nivoSlider">
                <img src="resources/images/main_image01.jpg" width="1000" height="500" alt="" title="" />
                <img src="resources/images/main_image02.jpg" width="1000" height="500" alt="" title="" />
                <img src="resources/images/main_image03.jpg" width="1000" height="500" alt="" title="" />
                <img src="resources/images/main_image04.jpg" width="1000" height="500" alt="" title="" />
            </div>
            </div>  
                
            </div>	
            
                        
        <div class="homeOurProductsMain">
        	<h2><div class="homeOurProductsTopic">OUR PRODUCTS</div></h2>
            
            <div class="homeProductsSet">
            	<div class="floatLeft"><a href="products/rubber_bands.php"><img src="resources/images/products_rubberband.jpg" width="184" height="120" alt="" /></a></div>
              <h2><a href="products/rubber_bands.php">Rubber Bands</a></h2>
              <p>Stationary, Postal, Ceramic, Vegetable, Agricultural & Exercise Bands</p>
                
            </div>
            
            <div class="homeProductsSet">
            	<div class="floatLeft"><a href="products/pallets_mover_bands.php"><img src="resources/images/products_moverband.jpg" width="184" height="120" alt="" /></a></div>
                <h2><a href="products/pallets_mover_bands.php">Pallet & Mover Bands</a></h2>
                <p>Best performing bands in the market</p>
                
            </div>
            
            <div class="homeProductsSet">
            	<div class="floatLeft"><a href="products/straps_main.php"><img src="resources/images/products_straps.jpg" width="184" height="120" alt="" /></a></div>
                <h2><a href="products/straps_main.php">Straps</a></h2>
                <p>For truck covers, tents, boat covers, pool covers & other industrial applications</p>
                
            </div>
            
            <div class="homeProductsSet">
            	<div class="floatLeft"><a href="products/industrial_products.php"><img src="resources/images/products_industrial.jpg" width="184" height="120" alt="" /></a></div>
                <h2><a href="products/industrial_products.php">Industrial Products</a></h2>
                <p>Moulded & extruded rubbers parts for bellows, dampers, seals, rubber ropes, staircase strips</p>
                
            </div>
            
            <div class="homeProductsSet" style="margin:0px;">
            	<div class="floatLeft"><a href="products/household_products.php"><img src="resources/images/products_household.jpg" width="184" height="120" alt="" /></a></div>
                <h2><a href="products/household_products.php">Household Products</a></h2>
                <p>Door stoppers, door wedges, sink covers, dolly cap covers</p>
                
            </div>
        	
        </div>
        
        <div class="floatLeft">
            <div class="homeBottomAbout">
				<h2><div class="homeBottomAboutTopic">ABOUT US</div></h2>
                <div class="homeBottomAboutText">
                Our team enjoys working with our customers and selected development partners to evolve products to meet customer needs. We are committed to provide cost effective, superior quality solutions.
                <a href="about/index.php">How it works?</a>
                </div>	            
            </div>
            
            <div class="homeBottomQuality">
				<h2><div class="homeBottomQualityTopic">QUALITY</div></h2>
                <div class="homeBottomQualityText">
                <img src="resources/images/home_quality_image.gif" width="155" height="86" alt="" />
                In accordance with global quality standards, our facilities are ISO 9001 certified by SGS UK. <a href="quality/index.php">Read more</a>
                </div>	            
            </div>
            
            <div class="homeBottomNews">
				<h2><div class="homeBottomNewsTopic">NEWS</div></h2>
                <div class="homeBottomNewsText">
                	<ul>
                		<li><a href="#">World Rubber Summit - 2012 to be held in Singapore from 13th July 2012 to 27th July 2012 ....</a></li>
                        <li><a href="#">JAF RUBBER complaint with all SEDEX ....</a></li>
                    </ul>
                <a href="#" style="padding:0px;">Read more</a>
                </div>	            
            </div>
            
            
            
        </div>
        </div>
        
        <?php include("includes/footer.php"); ?>  
        
    
</div>

</body>
</html>