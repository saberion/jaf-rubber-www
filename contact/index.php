<?php
ob_start();
session_start();
include('tomail.php');
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="keywords" content="Rubber, rubber products, rubber bands, pallet & mover bands, Straps,  sri lankan rubber manufacturer "/>
<meta name="description" content="JB Rubber Products was incorporated in the early 80's to manufacture extruded rubber products such as rubber bands and has since evolved into a dynamic manufacturer of both extruded and moulded products.">
<meta name="author" content="">
<link rel="icon" href="../resources/images/favicon.ico">

<title>JAF RUBBER | Rubber Products Manufacturer - Sri Lanka | Contact Us</title>

<link href="../resources/styles/main.css" type="text/css" rel="stylesheet" media="screen" />

<script type="text/javascript" src="../resources/scripts/jquery-1.7.1.min.js"></script>
<link rel="stylesheet" type="text/css" href="../resources/styles/ddsmoothmenu.css" />

<script type="text/javascript" src="../resources/scripts/ddsmoothmenu.js">
/***********************************************
* Smooth Navigational Menu- (c) Dynamic Drive DHTML code library (www.dynamicdrive.com)
* This notice MUST stay intact for legal use
* Visit Dynamic Drive at http://www.dynamicdrive.com/ for full source code
***********************************************/
</script>
<script type="text/javascript">
ddsmoothmenu.init({
	mainmenuid: "smoothmenu1", //menu DIV id
	orientation: 'h', //Horizontal or vertical menu: Set to "h" or "v"
	classname: 'ddsmoothmenu', //class added to menu's outer DIV
	//customtheme: ["#1c5a80", "#18374a"],
	contentsource: "markup" //"markup" or ["container_id", "path_to_menu_file"]
})
</script>

<link rel="stylesheet" type="text/css" href="../resources/styles/products_rightmenu.css" />
<script type="text/javascript" src="../resources/scripts/ddaccordion.js">
/***********************************************
* Accordion Content script- (c) Dynamic Drive DHTML code library (www.dynamicdrive.com)
* Visit http://www.dynamicDrive.com for hundreds of DHTML scripts
* This notice must stay intact for legal use
***********************************************/
</script>
<script type="text/javascript">
ddaccordion.init({
	headerclass: "submenuheader", //Shared CSS class name of headers group
	contentclass: "submenu", //Shared CSS class name of contents group
	revealtype: "click", //Reveal content when user clicks or onmouseover the header? Valid value: "click", "clickgo", or "mouseover"
	mouseoverdelay: 200, //if revealtype="mouseover", set delay in milliseconds before header expands onMouseover
	collapseprev: true, //Collapse previous content (so only one open at any time)? true/false 
	defaultexpanded: [], //index of content(s) open by default [index1, index2, etc] [] denotes no content
	onemustopen: false, //Specify whether at least one header should be open always (so never all headers closed)
	animatedefault: false, //Should contents open by default be animated into view?
	persiststate: false, //persist state of opened contents within browser session?
	toggleclass: ["", ""], //Two CSS classes to be applied to the header when it's collapsed and expanded, respectively ["class1", "class2"]
	togglehtml: ["suffix", "<img src='../resources/images/plus.gif' class='statusicon' />", "<img src='../resources/images/minus.gif' class='statusicon' />"], //Additional HTML added to the header when it's collapsed and expanded, respectively  ["position", "html1", "html2"] (see docs)
	animatespeed: "fast", //speed of animation: integer in milliseconds (ie: 200), or keywords "fast", "normal", or "slow"
	oninit:function(headers, expandedindices){ //custom code to run when headers have initalized
		//do nothing
	},
	onopenclose:function(header, index, state, isuseractivated){ //custom code to run whenever a header is opened or closed
		//do nothing
	}
})
</script>

</head>

<body>
<div class="mainWrapper">
	<div class="mainHeader">
		<?php include("../includes/header_inner.php"); ?>
    </div>
    
    <div class="contAreaMainWrapper">
    
    <div class="innerPagesMainDiv">
   	  <div class="breadcrumbDiv">
        You are here :&nbsp; <span>Home</span> Contact Us
      </div>
        
        <div class="productsPageMaindiv">
       	  <div class="productsLeftNavMain">
          
          <div class="glossymenu">
            <a class="menuitem" href="../about/index.php">About Us</a>
            <a class="menuitem" href="../news/index.php">News</a>
            <a class="menuitem productsLeftListSelect" href="index.php">Contact Us</a>
            </div>
          	
          </div>
            
            <div class="productsRightMain">
           	  <div class="productDescTopMain">
                
                <div class="productDescTopRight" style="width:770px; padding:0px;">
                	<div class="insideTopImage"><img src="../resources/images/contact_image.jpg" width="770" height="260" alt="" /></div>
                    
                    
                    <div class="contactInfoDiv">
                    <h2>Jafferjee Brothers Exports (Pvt) Ltd</h2>
                    
                  	<p>No. 150, St. Joseph's Street,<br />
                    Colombo 14,<br />
                    Sri Lanka.</p>
                    <ul>
                        <li><img src="../resources/images/contact_phone.png" width="32" height="20" alt="" /> +94-11-4606200</li>
                        <li><img src="../resources/images/contact_fax.png" width="32" height="20" alt="" /> +94-11-2446085</li>
                        <li><img src="../resources/images/contact_email.png" width="32" height="20" alt="" /> <a href="mailto:jbrbf@slt.lk" target="_blank">jbrbf@slt.lk</a></li>
                    </ul>
					<p>&nbsp;</p>
                    <p><iframe src="https://www.google.com/maps/embed?pb=!1m14!1m12!1m3!1d990.1268335655438!2d79.87379926087642!3d6.9493233596110136!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!5e0!3m2!1sen!2s!4v1397804322985" width="360" height="250" frameborder="0" style="border:0"></iframe></p>
                    
                    </div>
                    
                    <div class="contactForm">
                    <h3><div class="keyAdvantagesTopic" style="width:105px;">CONTACT FORM</div></h3>
                    	<ul>
                        	<form action="<?php echo $_SERVER['PHP_SELF']; ?>" method="post" enctype="multipart/form-data">
                 	
                    		<li>
                            <div style="float:left; font-size:12px; color:#CC3333; font-family:Arial, Helvetica, sans-serif">												 							<?php if(!empty($accept)) echo  $accept ?>
							<?php if(!empty($error)) echo  $error  ?></div>
                            </li>
                        	<li><label>Name</label> <input name="name" id="name" type="text" class="contactTextArea" /></li>
                            <li><label>Email</label> <input name="email" id="email" type="text" class="contactTextArea" /></li>
                            <li><label>Subject</label> <input name="subject" id="subject" type="text" class="contactTextArea" /></li>
                            <li><label>Telephone</label> <input name="phone" id="phone" type="text" class="contactTextArea" /></li>
                            <li><label>Message</label> <textarea name="comment" id="comment" type="text" class="contactTextArea" style="height:80px;" > </textarea></li>
                            
                            <li><label>Enter this code here</label><img src="../captcha/captcha_class.php" id="docaptcha" onclick="this.src='../captcha/captcha_class.php?'+Math.random();" style="cursor:hand; height:32px; float:left;" title="Click to refresh code"> <input name="" type="text" class="contactTextArea" style="width:136px; margin-left:20px;"/></li>
                            
                            <li><label>&nbsp;</label><input type="submit" name="submit" value="SUBMIT" class="submitBtn"/></li>
                            </form>
                        </ul>
                    </div>
                                 
                </div>
              </div>
              
              
              
            </div>
            
            
        
        </div>		
            
                        
        
    </div>     
        
  </div>
        
    <?php include("../includes/footer.php"); ?>  
        
    
</div>




</body>
</html>